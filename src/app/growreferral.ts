export class GrowReferral {
    public tokenExpiry = 60*60*1; //1 day for testing (60*60*30); //30 days for prod
    constructor() {

    }//end function

    doWeNeedToUpdateToken (lastupdated){
      return (lastupdated + this.tokenExpiry) < this.getTimeStampNow();
    }

    getTimeStampNow (){
      return Math.floor(Date.now() / 1000);
    }//end function

    //mytodo this func should go in user
    generate_token(length){
        //generate token
        var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
        var b = [];
        for (var i=0; i<length; i++) {
            var j = (Math.random() * (a.length-1)).toFixed(0);
            b[i] = a[j];
        }
        return b.join("");
    }//end function

    //This function is used by to redirect user to correct page based on whether there is active level or club associated with him/her
    whereToGo (resp_getCacheUsers){
            if(resp_getCacheUsers.is_active_level != "0"){
                    return  'TabsPage';
            } else {
                    //User do not have correct active subscription
                    //this.navCtrl.setRoot(StatusPage, {"code":1});
                    console.log ("Go to Status");
                    return  'StatusPage';
            }//endif
    }//end function

}//end class
