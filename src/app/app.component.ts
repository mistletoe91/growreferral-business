import {  Config, Nav, Platform } from 'ionic-angular';

import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';

import { User } from '../providers';
import { CONST} from './const';

//mytodo : remove mock going in production
import { SQLite, SQLiteDatabaseConfig   } from '@ionic-native/sqlite';
import { SQLiteMock, SQLiteObject, SQLitePorterMock  } from '../mocks/providers/sqllite';

//Load GrowReferral class
import { GrowReferral } from './growreferral';

//Load Pages
import { FirstRunPage,LoginPage  } from '../pages';
import {StatusPage} from '../pages/status/status';
import {ChooseClubPage} from '../pages/chooseclub/chooseclub';


import { Settings } from '../providers';

@Component({
  template: `<ion-menu [content]="content">
    <ion-header>
      <ion-toolbar>
        <ion-title>Pages</ion-title>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-list>
        <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
          {{p.title}}
        </button>
      </ion-list>
    </ion-content>

  </ion-menu>
  <ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  public objGrowReferral:any = new GrowReferral();


  rootPage = FirstRunPage;
  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Home', component: 'ListMasterPage' },
    { title: 'Settings', component: 'SettingsPage' },
    { title: 'Payment', component: 'ListMasterPage' },
    { title: 'Help', component: 'ListMasterPage' },
    { title: 'Support', component: 'ListMasterPage' },
    { title: 'About', component: 'ListMasterPage' },
  ]

  tokenOperation() {
      this.sqlite.create({
        name: 'gr.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql(CONST.create_table_statement, {})
        .then(res => {
            db.executeSql("SELECT * FROM cache_users_local", {})
            .then(res => {
                console.log (res.rows.item(0));
                if(res.rows.length>0){
                        //console.log ("Record Exists");

                         //No token or UserID ? Go to Login --- This should not happen
                        if(!res.rows.item(0).apptoken || !res.rows.item(0).wp_users_id){
                              this.rootPage = LoginPage;
                              return;
                        }//endif

                        //Update User class so we know which token we have
                        this.user.setToken(res.rows.item(0).apptoken);
                        this.user.setUID(res.rows.item(0).wp_users_id);

                        //First Get user Details form MYSQL so we know what we have
                        this.user.getCacheUsers().subscribe((resp_getCacheUsers) => {

                                    //Just to make sure user exists
                                    if (resp_getCacheUsers.length>0 && res.rows.item(0).wp_users_id>0){

                                           //just FYI - user found but can have is_active_level either set to 0 or 1

                                           //Check if we need to update toke ( i.e. if it got expired )
                                           if(this.objGrowReferral.doWeNeedToUpdateToken (res.rows.item(0).lastupdated)){

                                                  //Yes we need to update token
                                                 this.user.updateToken(this.objGrowReferral.generate_token (30)).subscribe((resp_updateToken_1) => {
                                                          if(resp_updateToken_1){
                                                              db.executeSql("UPDATE cache_users_local SET apptoken='"+this.user.getToken()+"',lastupdated='"+Math.floor(Date.now() / 1000)+"' where wp_users_id="+res.rows.item(0).wp_users_id , {})
                                                              .then(res => {
                                                                          //All Tokens Good
                                                                          this.rootPage = this.objGrowReferral.whereToGo (resp_getCacheUsers) ;
                                                              })
                                                              .catch(e => {
                                                                          //mytodo : handle exception
                                                                          this.rootPage = LoginPage;
                                                              });
                                                          }//endif
                                                  });//update the token -   Operation
                                           } else {
                                                     //console.log ("No need to update token");
                                                    this.rootPage = this.objGrowReferral.whereToGo (resp_getCacheUsers) ;
                                           }//endif

                                    } else {
                                            console.log ("user either got  deleted or token mismatch");
                                            this.rootPage = LoginPage;
                                    }//endif


                        });//end function : getCacheUsers

                } else {
                        //Record Not exist so the user should go to login page
                        this.rootPage = LoginPage;

                }//endif
            })
            .catch(e => {
                //Error in operation
            });

        })
        .catch(e => {
            //Error in operation
        });//create table

      });//create database
 }//end function

  constructor(
    private translate: TranslateService,
    platform: Platform,
    settings: Settings,
    private config: Config,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    public user: User,
    private sqlite: SQLite
    ) {
    platform.ready().then(() => {

      this.tokenOperation ();

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      console.log ("AppComponent is done");
    });
    this.initTranslate();
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'zh') {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use('zh-cmn-Hans');
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use('zh-cmn-Hant');
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
