import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User } from '../../providers';
import { CONST} from '../../app/const';

//Load GrowReferral class
import { GrowReferral } from '../../app/growreferral';

//Load Pages
import { ChooseClubPage } from '../chooseclub/chooseclub';
import { TabsPage } from '../tabs/tabs';
import {StatusPage} from '../status/status';

//mytodo : remove mock going in production
import { SQLite, SQLiteDatabaseConfig   } from '@ionic-native/sqlite';
import { SQLiteMock, SQLiteObject, SQLitePorterMock  } from '../../mocks/providers/sqllite';

@IonicPage() 
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public objGrowReferral:any = new GrowReferral();


  account: { username: string, password: string, apptoken: string } = {
    username: '1@gr.com',
      password: '123456',
    apptoken : this.objGrowReferral.generate_token (30)
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    private sqlite: SQLite,
    public translateService: TranslateService) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  tokenOperation(resp, callback) {
      this.sqlite.create({
        name: 'gr.db',
        location: 'default'
      }).then((db: SQLiteObject) => {

        /*Start Create Table*/
        db.executeSql(CONST.create_table_statement, {})
        .then(res => {
            //Control here : new table created or not
            //console.log ("Table created");
            db.executeSql('DELETE FROM cache_users_local', {}).then(res => {

                              db.executeSql("INSERT INTO cache_users_local ('wp_users_id', 'apptoken', 'lastupdated') VALUES ("+resp.user_i+", '"+this.account.apptoken+"' , '"+Math.floor(Date.now() / 1000)+"')",[])
                                 .then(res => {
                                   //Update User class
                                   /*
                                   this.user.setToken(this.appTokenNew);
                                   this.user.setUID(resp.user_i);
                                   this.user.setCatId(resp.category_id);
                                   this.user.setPostal(resp.postal);
                                   this.user.setFirstName(resp.first_name);
                                   this.user.setLastName(resp.last_name);
                                   this.user.setClubId(resp.club_id);
                                   */

                                   //Now redirect to correct page
                                   console.log ("This Works");
                                   this.navCtrl.setRoot(this.objGrowReferral.whereToGo (resp));
                                   //this.navCtrl.setRoot (ChooseClubPage);

                               })
                               .catch(e => {
                                 console.log ("ERROR ON INSERTION");
                                 console.log (e);
                                   //Error in operation
                                }); //Record inserted in local


            //End Deleting from cache_users_local
            })
            .catch(e => {
                //Error in operation
                console.log("Error deleting from Table");
                console.log (e);
            });
        })
        .catch(e => {
            //Error in operation
            console.log("Error creating Table");
            console.log (e);
        });//create table
        /*End   Create Table */


      });//create database
 }//end function


  // Attempt to login in through our User service
  doLogin() {
    this.user.login(this.account).subscribe((resp) => {
        //this.navCtrl.push(MainPage);

        //Login Sucessfull
        //console.log (resp);
        this.tokenOperation (resp,
          function() {
              //for future example only
          }
        );


    }, (err) => {
      //this.navCtrl.push(MainPage);
      // Unable to log in
      if(err.error.code == "invalid_email"){
        let toast = this.toastCtrl.create({
          message: "Seems like you are new user. You need to go to GrowReferral.com to register in order to use this APP",
          duration: 3000,
          position: 'top'
        });
        toast.present();
      } else {
        let toast = this.toastCtrl.create({
          message: this.loginErrorString,
          duration: 3000,
          position: 'top'
        });
        toast.present();
      }//endif

    });
  }
}
