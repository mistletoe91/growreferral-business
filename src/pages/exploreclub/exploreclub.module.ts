import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ExploreClubPage } from './exploreclub';

@NgModule({
  declarations: [
    ExploreClubPage,
  ],
  imports: [
    IonicPageModule.forChild(ExploreClubPage),
    TranslateModule.forChild()
  ],
  exports: [
    ExploreClubPage
  ]
})
export class ExploreClubPageModule { }
