import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ChooseClubPage } from './chooseclub';

@NgModule({
  declarations: [
    ChooseClubPage,
  ],
  imports: [
    IonicPageModule.forChild(ChooseClubPage),
    TranslateModule.forChild()
  ],
  exports: [
    ChooseClubPage
  ]
})
export class ChooseClubPageModule { }
