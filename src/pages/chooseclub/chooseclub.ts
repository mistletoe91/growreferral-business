import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform,App } from 'ionic-angular';
import { Requests, User } from '../../providers';
import { LoginPage,CreateClubPage  } from '../';

@IonicPage()
@Component({
  selector: 'page-chooseclub',
  templateUrl: 'chooseclub.html',
})
export class ChooseClubPage {
  public myRequests: any=[];

  doFindRequests (){

    //Get all Requests associated with this userId : Basically we pull from phone# stored in mysql against this userid
     this.requests.getClubRequests(this.user.getUID(),this.user.getToken() ).subscribe((resp) => {
            for(var i in resp){
                this.myRequests.push ( resp[i] );
            }//end end

      }, (err) => {
      });

  }//end function

  constructor(
    public requests: Requests,
    platform: Platform,
    public user: User,
    public navCtrl: NavController
  )
  {
    platform.ready().then(() => {
          console.log ("myRequests" + this.myRequests.length);
          if(this.user.getUID() && this.user.getToken()){
                  this.doFindRequests ();
          } else {
                  //This should not happen. This means we are missing UID or Token.If this happen - ask user to login
                  this.navCtrl.setRoot(LoginPage);
          }//endif


    });

  }

  fnGoToCreateClub() {
      this.navCtrl.setRoot(CreateClubPage);
  }

  fnBrowseClubs() {
    // this.navCtrl.setRoot(TabsPage);
  }

  fnViewClub() {

  }//end function

}//end class
