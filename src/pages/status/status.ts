import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the StatusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-status',
  templateUrl: 'status.html',
})
export class StatusPage {
  public slide_title;
  public slide_description;
  constructor(public navCtrl: NavController, public navParams: NavParams,translate: TranslateService) {

    translate.get(["status_page_code_1_title",
      "status_page_code_1_desc"
    ]).subscribe(
      (values) => {
        console.log('Loaded values', values);
        switch (navParams.get('code')) {
                    case 1:
                        console.log ("aaaaaaaaaaaa");
                        this.slide_title  = values.status_page_code_1_title;
                        this.slide_description= values.status_page_code_1_desc;
                        break;
                    default:

        }//end switch

    });
/*
          */

  }//end constructor

  ionViewDidLoad() {
    console.log('ionViewDidLoad StatusPage');
  }

}
