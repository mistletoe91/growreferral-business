import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { IonicPage, NavController, NavParams,Platform,App } from 'ionic-angular';
import { Clubs, User } from '../../providers';
import { LoginPage  } from '../';
 //s
@Component({
  selector: 'page-create-club',
  templateUrl: 'create-club.html',
})
export class CreateClubPage {

  constructor(
    platform: Platform,
    public clubs:Clubs,
    private formBuilder: FormBuilder,
    public toastCtrl: ToastController,
    public user: User,
    public navCtrl: NavController
  )
  {
    platform.ready().then(() => {
          if(this.user.getUID() && this.user.getToken()){

          } else {
                  //This should not happen. This means we are missing UID or Token.If this happen - ask user to login
                  this.navCtrl.setRoot(LoginPage);
          }//endif


    });

  }


}//end class
