import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ClubsPage } from './clubs';

@NgModule({
  declarations: [
    ClubsPage,
  ],
  imports: [
    IonicPageModule.forChild(ClubsPage),
    TranslateModule.forChild()
  ],
  exports: [
    ClubsPage
  ]
})
export class ClubsPageModule { }
