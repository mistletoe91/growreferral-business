import 'rxjs/add/operator/toPromise';
import { Injectable } from '@angular/core';
import { Api } from '../api/api';

@Injectable()
export class User {
  _user: any;
  public uid;
  public myClubId;
  public firstName;
  public lastName;
  public catId;
  public postal;
  public token;
  public is_active_level;

  /*Injectable Start*/
  setToken (token_) {
    this.token = token_;
  }
  getToken() {
    return this.token;
  }

  setUID (id_) {
    this.uid = id_;
  }
  getUID() {
    return this.uid;
  }

  setCatId (catId_) {
    this.catId = catId_;
  }
  getCatId() {
    return this.catId;
  }

  setPostal (postal_) {
    this.postal = postal_;
  }
  getPostal() {
    return this.postal;
  }

  setFirstName (name) {
    this.firstName = name;
  }
  getFirstName() {
    return this.firstName;
  }

  setLastName (name) {
    this.lastName = name;
  }
  getLastName() {
    return this.lastName;
  }

  setClubId(newclubId) {
    this.myClubId = newclubId;
  }
  getClubId() {
    return this.myClubId;
  }

  setIsActiveLevel(level_) {
    this.is_active_level = level_;
  }
  getIsActiveLevel() {
    return this.is_active_level;
  }

 /*Injectable End*/

  constructor(public api: Api) { }

  /**
   * Update token preiodicly
   */
  getCacheUsers() {
    console.log ("getCacheUsers");
    console.log (this);
    if(!this.token){
      return ;
    }//endif
    if(!this.uid){
      return ;
    }//endif
    let seq = this.api.post('/wp-json/growreferral/v1/getcacheusers', {"i":this.uid,"token":this.token} ).share();
    seq.subscribe((res: any) => {
              this.setCatId(res.category_id);
              this.setPostal(res.postal);
              this.setFirstName(res.first_name);
              this.setLastName(res.last_name);
              this.setClubId(res.club_id);
    }, err => {
            console.error('ERROR', err);
    });
    return seq;
  }

  setAll (res){ 

    //this.setUID(res.user_i);
    this.setCatId(res.category_id);
    this.setPostal(res.postal);
    this.setFirstName(res.first_name);
    this.setLastName(res.last_name);
    this.setClubId(res.club_id);

  }

  /**
   * Update token preiodicly
   */
  updateToken(newToken) {

    //Prepare object
    let accountInfo: { n: string, o: string, i: number  } = {
      n : newToken,
      o : this.getToken (),
      i : this.getUID ()
    };

    let seq = this.api.post('/wp-json/growreferral/v1/updateusertoken', accountInfo).share();

    seq.subscribe((res: any) => {
        this.setToken(accountInfo.n);
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        //this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }



  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    let seq = this.api.post('/wp-json/jwt-auth/v1/token', accountInfo).share();

    seq.subscribe((res: any) => {
        console.log ("AFTER LOGIN : res");
        console.log (res);
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this.setAll(res);
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {
    let seq = this.api.post('signup', accountInfo).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this._user = null;
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(resp) {
    this._user = resp.user;
  }
}
