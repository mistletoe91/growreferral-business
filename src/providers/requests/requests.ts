import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';


@Injectable()
export class Requests {
  public id;
  public from_wp_users_id;
  public send_at;
  public club_id;
  public send_to_phone;

  /*Injectable Start*/
  setID (id_) {
    this.id = id_;
  }
  getID() {
    return this.id;
  }

  setFrom_wp_users_id (from_wp_users_id_) {
    this.from_wp_users_id = from_wp_users_id_;
  }
  getFrom_wp_users_id() {
    return this.from_wp_users_id;
  }

  setSendAt (send_at_) {
    this.send_at = send_at_;
  }
  getSendAt() {
    return this.send_at;
  }

  setClub_id (club_id_) {
    this.club_id = club_id_;
  }
  getClub_id() {
    return this.club_id;
  }

  setSendToPhone (send_to_phone_) {
    this.send_to_phone = send_to_phone_;
  }
  getSendToPhone() {
    return this.send_to_phone;
  }
 /*Injectable End*/

  constructor(public api: Api) { }

  setAll (res){
    this.setID(res.id);
    this.setFrom_wp_users_id(res.from_wp_users_id);
    this.setSendAt(res.send_at);
    this.setClub_id(res.club_id);
    this.setSendToPhone(res.send_to_phone);
  }

  //get requests send to me
  getClubRequests(uid, token_) {
    let seq = this.api.post('/wp-json/growreferral/v1/getclubjoinrequests', {"i1":uid,"token":token_} ).share();
    seq.subscribe((res: any) => {
            console.log ("Get Requests");
            console.log (res);
            //this.setAll (res);
    }, err => {
            console.error('ERROR', err);
    });
    return seq;
  }//end function

}
