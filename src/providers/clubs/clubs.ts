import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';

@Injectable()
export class Clubs {
  _user: any;
  public id;
  public name;
  public description;
  public admin_user_id;

  /*Injectable Start*/
  setUID (id_) {
    this.id = id_;
  }
  getUID() {
    return this.id;
  }

  setAdmin_user_id (admin_user_id_) {
    this.admin_user_id = admin_user_id_;
  }
  getAdmin_user_id() {
    return this.admin_user_id;
  }

  setName (name_) {
    this.name = name_;
  }
  getName() {
    return this.name;
  }

  setDesc (desc_) {
    this.description = desc_;
  }
  getDesc() {
    return this.description;
  }
 /*Injectable End*/

  constructor(public api: Api) { }



  setAll (res){
    this.setUID(res.id);
    this.setAdmin_user_id(res.admin_user_id);
    this.setName(res.name);
    this.setDesc(res.description);
  }

  /**
   * Update token preiodicly
   */
  getClubsNearMe(uid, token_) {
    let seq = this.api.post('/wp-json/growreferral/v1/getclubs', {"uid":uid,"token":token_} ).share();
    seq.subscribe((res: any) => {
            console.log ("Get Clubs");
            this.setAll (res);
    }, err => {
            console.error('ERROR', err);
    });
    return seq;
  }

}
