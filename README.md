
DATABASE
------------->
Mysqli
    contacts:::
        - Phone, Name,  USER_ID
    contacts_details (This table refresh once a day ) :::
          USER_ID, business Name, Business Category, Postal , Country  

MYSQL
    users:::
        - USER_ID, Phone, Name, FriendRequestStatus ( 0=none, 1=send, 2=accepted), Business Name, Business Category, Postal , Country, is_active_level

    request ::::
          ID, USER_ID (from), USER_ID (to)

PAGES
------------->
TABS
        - Contacts
                 - View all contacts
                 - Send Friend Request
                 - Add New Contact (Modal)   
                 - Send Referral (Modal)   
        - Referrals
                 - Approve/Reject Referral
                 - View Referral (Modal)
        - Stats
                 - View aggregriate of Referrals
        - Search
                 - View Profile (New Page)

--------------
WORKFLOW
------------->
- "MAIN"
  --------------------------
- If cache_users_local has Record
    - if Token Expired  
          - Generate New Token
                - Update cache_users_local with new token
                - Update mysql with new token
                - (2) Redirect to NEXT page
    - ELSE
          - (2) Redirect to Next page
- Else
    - Go to login  
          - Generate New Token          
          - Authenticate ( and send new appToken )
              - User/Pass success
                  - Update cache_users_local with new token
                    - (2) Redirect to Next page


(2) "REDIRECT TO NEXT PAGE"
  -----------------------
  Check Mysql.Requests
      In Requests : ( Select * from requests where to = MY_MSQL_ID and request.status NOT approved )
      Friend`s List : ( Select * from request where (to=MYSQL_ID AND status=approved) OR (from=MYSQL_ID)  )
      Out Requests: : ( Select * from request where (from=MYSQL_ID) )



 WEBSITE
 ----------------------
1 :
    Phone Number
    Password

2 :
    Your Name
    Business Name
    Business Category

3 :
    Credit Card
    Postal
    Country

---------------------

      If guest
            Home page shows registration page and login link
      If registered user
            if has active subscription
                 Home page shows ACCOUNT_PAGE
            else
                 Home page shows CREATE_SUBSCRIPTION
